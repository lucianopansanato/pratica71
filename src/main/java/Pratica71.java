
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;
import utfpr.ct.dainf.if62c.pratica.Jogador;
import utfpr.ct.dainf.if62c.pratica.JogadorComparator;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author LucianoTadeu
 */
public class Pratica71 {
    public static void main(String[] args) {
        // Solicite que o usuário digite o número de jogadores a serem lidos
        Scanner scanner = new Scanner(System.in);
        int numJogadores;
        System.out.print("Digite o número de jogadores: ");
        numJogadores = scanner.nextInt();
        System.out.println(numJogadores);
        
        // Execute um laço e leia o número e nome de cada jogador. Para cada 
        // leitura crie um Jogador e armazene na lista. Na leitura dos dados, 
        // verifique que o usuário digitou um número válido antes de tentar 
        // ler o dado, evitando que o programa aborte.
        List<Jogador> lst = new ArrayList<>();
        int numero = 0;
        String nome;
        for (int i = 0; i < numJogadores; i++) {
            do {
                System.out.print("Digite o número do jogador: ");
                if (scanner.hasNextInt()) {
                    numero = scanner.nextInt();
                    break;
                } else {
                    scanner.next();
                    System.out.println("Digite um número!");
                }
            } while (true);
            System.out.print("Digite o nome do jogador: ");
            nome = scanner.next();
            //System.out.println(numero + " " + nome);
            lst.add(new Jogador(numero, nome));
        }
        
        // Ordene a lista de jogadores por ordem crescente de número e exiba a lista.
        JogadorComparator cmpAscNum = new JogadorComparator(true, true, false);
        Collections.sort(lst,cmpAscNum);
        System.out.println("Time em Ordem Ascendente de Número");
        for (Jogador jog: lst) {
            System.out.println(jog);
        }
        
        // Depois de exibir a lista de jogadores, de acordo com o item 5, 
        // execute um laço que permita ao usuário incluir mais jogadores na 
        // lista existente. O laço deve terminar quando o usuário digitar 
        // 0 (zero) para o número do jogador. Caso um jogador com o número 
        // informado já exista na lista, o nome do jogador deverá ser 
        // atualizado e caso não exista, o jogador deve ser inserido na 
        // posição correta de modo a que a lista continue ordenada. Após cada 
        // leitura, exiba a lista novamente verificando que esta permanece ordenada.
        
        numero = 0;
        Jogador j = new Jogador(0, ""); int indice;
        do {
            do {
                System.out.print("Digite o número do jogador: ");
                if (scanner.hasNextInt()) {
                    numero = scanner.nextInt();
                    break;
                } else {
                    scanner.next();
                    System.out.println("Digite um número!");
                }
            } while (true);
            if (numero == 0) {
                continue;
            }
            System.out.print("Digite o nome do jogador: ");
            nome = scanner.next();
            
            j.setNumero(numero);
            j.setNome(nome);
            indice = Collections.binarySearch(lst, j, cmpAscNum);
            //System.out.println(indice);
            
            if (indice < 0) { // Jogador não existe na lista
                // Insere jogador
                //System.out.println((-indice)-1);
                lst.add((-indice)-1, new Jogador(numero, nome));
            } else {
                // Atualiza nome
                lst.get(indice).setNome(nome);
            }
            
            // Exibir a lista
            System.out.println("Time em Ordem Ascendente de Número");
            for (Jogador jog: lst) {
                System.out.println(jog);
            }       
        } while (numero != 0);
    }
}
